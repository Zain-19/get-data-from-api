import 'package:flutter/material.dart';
import 'Services.dart';
import 'Post.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Get Data',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Post> posts;
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    Services.getPost().then((list) {
      setState(() {
        posts = list;
      });
    });

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Text("Fetch Data From API",style: TextStyle(fontSize: 30.0, color: Colors.black),),
        centerTitle: true,
      ),
    body: Padding(
      padding: const EdgeInsets.symmetric(vertical: 20.0),
      child: Container(
      child: ListView.builder(
      itemCount: posts == null ? 0 : posts.length,
      itemBuilder: (context, int index){
        Post post = posts[index];
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          child: Card(
            color: Colors.amber[200],
            shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),),
            child: Padding(
             padding: const EdgeInsets.symmetric(vertical: 10.0, ),
              child: ListTile(    
              title: Text(" Name: ${post.name}\n Email: ${post.email}\n Website: ${post.website}\n Company: ${post.company.name}\n Phone: ${post.phone}" , style: TextStyle(fontSize: 30.0),),
    
          ),
            )
          ),
        );
      }   
      )
    ),
    ),
    );
  }
}





