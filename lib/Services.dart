import 'package:http/http.dart' as http;
import 'Post.dart';

class Services {
 static final url ="https://jsonplaceholder.typicode.com/users";

static Future<List<Post>> getPost()async{
try{
  final response = await http.get(url);
  if(response.statusCode == 200){
    print("Request Successfull...");
    final List<Post> listPosts = postFromJson(response.body);
    return listPosts;
  }
}
catch(e){
  return List<Post>();
}
}
}